#!/bin/bash

# Atualiza pacote soma números
# Author: luancarloswd

PACOTE="SomaNumeros"
VERSAO_ATUALIZADA_PROJETO=${CI_COMMIT_TAG}

# Definir usuário git para utilizar no commit de atualização
git config --global user.email "$GIT_USER_EMAIL"
git config --global user.name "$GIT_USER_NAME"

MERGE_REQUEST_BRANCH="feature/atualizacao_${PACOTE}_${VERSAO_ATUALIZADA_PROJETO}" # Branch para atualização
MENSAGEM_COMMIT="Atualizando versão ${PACOTE}: ${VERSAO_ATUALIZADA_PROJETO}"     # Mensagem para o commit

# Obter projetos em que vamos atualizar
# Podemos utilizar aqui um parâmetro da api "search" caso tenha um padrão de nomenclatura
# https://docs.gitlab.com/ee/api/projects.html
PROJETOS=$(curl --location --request GET "https://gitlab.com/api/v4/projects?owned=true&order_by=name&per_page=10000&private_token=${GITLAB_TOKEN}")

for projeto in $(echo "${PROJETOS}" | jq -r '.[] | @base64'); do
  _jq() {
    echo "${projeto}" | base64 --decode | jq -r "${1}"
  }

  api_path=$(_jq '.path')
  url_repo=$(_jq '.http_url_to_repo')
  url_repo=${url_repo#"https://"} # Removemos o 'https://' pois vamos concatenar com o usuário e token para clonar o projeto
  echo "${api_path}"

  # Clonar projeto da origem
  git clone https://${GITLAB_USER}:${GITLAB_TOKEN}@${url_repo}
  cd "${api_path}" || break

  # Iterar sobre cada arquivo csproj
  for csproj in $(find . -print | grep -i '.*[.]csproj'); do
    # Alterar a versão do pacote
    sed -i '/\'${PACOTE}'/c\    <PackageReference Include="'${PACOTE}'" Version="'${VERSAO_ATUALIZADA_PROJETO}'" />' ${csproj}

    # Verificar se houve alterações na branch
    if [ -n "$(git status --porcelain)" ]; then
      git checkout -b "${MERGE_REQUEST_BRANCH}" # Criar branch para atualização
      git add *.csproj                          # Adicionar arquivos .csproj modificados
      git commit -m "${MENSAGEM_COMMIT}"        # Fazer um commit
      git push -u origin HEAD                   # Enviar a branch criada para origem

      # Realizar Merge Request
      curl -X POST \
        https://gitlab.com/api/v4/projects/$(_jq '.id')/merge_requests \
        -H "cache-control: no-cache" \
        -H "content-type: multipart/form-data" \
        -H "private-token: ${GITLAB_TOKEN}" \
        -F source_branch="${MERGE_REQUEST_BRANCH}" \
        -F target_branch="master" \
        -F "title=${MENSAGEM_COMMIT}" \
        -F remove_source_branch=true
    else
      echo "Projeto não precisou de atualização"
    fi

  done

  cd ..
done
