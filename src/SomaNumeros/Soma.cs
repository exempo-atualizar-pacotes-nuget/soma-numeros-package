﻿namespace SomaNumeros
{
    public static class Soma
    {
        public static int Somar(this int n1, int n2) => n1 + n2;
    }
}
